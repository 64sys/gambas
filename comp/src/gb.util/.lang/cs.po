#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:27 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

# gb-ignore
#: File.class:22
msgid "&1 B"
msgstr ""

# gb-ignore
#: File.class:24
msgid "&1 KiB"
msgstr ""

# gb-ignore
#: File.class:26
msgid "&1 MiB"
msgstr ""

# gb-ignore
#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr "Afrikánština (Jižní Afrika)"

#: Language.class:17
msgid "Arabic (Egypt)"
msgstr "Arabsky (Egypt)"

#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr "Arabsky (Tunisko)"

#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr "Ázerbájdžánsky (Ázerbájdžán)"

#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr "Bulharsky (Bulharsko)"

#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr "Katalánsky (Katalánsko, Španělsko)"

#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr "Velšsky (Velká Británie)"

#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr "Česky (Česká republika)"

#: Language.class:37
msgid "Danish (Denmark)"
msgstr "Dánsky (Dánsko)"

#: Language.class:40
msgid "German (Germany)"
msgstr "Německy (Německo)"

#: Language.class:41
msgid "German (Belgium)"
msgstr "Něměcky (Belgie)"

#: Language.class:44
msgid "Greek (Greece)"
msgstr "Řecky (Řecko)"

#: Language.class:47
msgid "English (common)"
msgstr "Anglicky (obecná anličtina)"

#: Language.class:48
msgid "English (United Kingdom)"
msgstr "Anglicky (Velká Británie)"

#: Language.class:49
msgid "English (U.S.A.)"
msgstr "Anglicky (U.S.A.)"

#: Language.class:50
msgid "English (Australia)"
msgstr "Anglicky (Austrálie)"

#: Language.class:51
msgid "English (Canada)"
msgstr "Anglicky (Kanada)"

#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr "Esperanto (kdekoli!)"

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

#: Language.class:58
msgid "Spanish (Spain)"
msgstr "Španělsky (Španělsko)"

#: Language.class:59
msgid "Spanish (Argentina)"
msgstr "Španělsky (Argentina)"

#: Language.class:62
msgid "Estonian (Estonia)"
msgstr "Estonsky (Estonsko)"

#: Language.class:65
msgid "Basque (Basque country)"
msgstr "Basque (Baskicko)"

#: Language.class:68
msgid "Farsi (Iran)"
msgstr "Perština (Írán)"

#: Language.class:71
msgid "Finnish (Finland)"
msgstr "Finsky (Finsko)"

#: Language.class:74
msgid "French (France)"
msgstr "Francouzsky (Francie)"

#: Language.class:75
msgid "French (Belgium)"
msgstr "Francouzsky (Belgie)"

#: Language.class:76
msgid "French (Canada)"
msgstr "Francouzsky (Kanada)"

#: Language.class:77
msgid "French (Switzerland)"
msgstr "Francouzsky (Švýcarsko)"

#: Language.class:80
msgid "Galician (Spain)"
msgstr "Galsky (Španělsko)"

#: Language.class:83
msgid "Hebrew (Israel)"
msgstr "Hebrejština (Izrael)"

#: Language.class:86
msgid "Hindi (India)"
msgstr "Hindština (Indie)"

#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr "Maďarsky (Maďarsko)"

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr "Chorvatsky (Chorvatsko)"

#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr "Indonésky (Indonézie)"

#: Language.class:98
msgid "Irish (Ireland)"
msgstr "Irsky (Irsko)"

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr "Islandsky (Island)"

#: Language.class:104
msgid "Italian (Italy)"
msgstr "Italsky (Itálie)"

#: Language.class:107
msgid "Japanese (Japan)"
msgstr "Japonsky (Japonsko)"

#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr "Khmer (Kambodža)"

#: Language.class:113
msgid "Korean (Korea)"
msgstr "Korejština (Korea)"

#: Language.class:116
msgid "Latin"
msgstr "Latina"

#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr "Litevsky (Litva)"

#: Language.class:122
msgid "Malayalam (India)"
msgstr "Malayalam (Indie)"

#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr "Makedonsky (Republika Makedonie)"

#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr "Holandsky (Nizozemí)"

#: Language.class:129
msgid "Dutch (Belgium)"
msgstr "Holandsky (Belgie)"

#: Language.class:132
msgid "Norwegian (Norway)"
msgstr "Norsky (Norsko)"

#: Language.class:135
msgid "Punjabi (India)"
msgstr "Punjabi (Indie)"

#: Language.class:138
msgid "Polish (Poland)"
msgstr "Polsky (Polsko)"

#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr "Portugalsky (Portugalsko)"

#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr "Portugalsky (Brazílie)"

#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr "Valencian (Valencian Community, Spain)"

#: Language.class:148
msgid "Romanian (Romania)"
msgstr "Rumunsky (Rumunsko)"

#: Language.class:151
msgid "Russian (Russia)"
msgstr "Rusky (Rusko)"

#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr "Slovinsky (Slovinsko)"

#: Language.class:157
msgid "Albanian (Albania)"
msgstr "Albánský (Albánie)"

#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr "Srbská (Srbsko & Černá Hora)"

#: Language.class:163
msgid "Swedish (Sweden)"
msgstr "Švédsky (Švédsko)"

#: Language.class:166
msgid "Turkish (Turkey)"
msgstr "Turecky (Turecko)"

#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr "Ukrajinsky (Ukrajina)"

#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr "Vietnamsky (Vietnam)"

#: Language.class:175
msgid "Wallon (Belgium)"
msgstr "Wallon (Belgie)"

#: Language.class:178
msgid "Simplified chinese (China)"
msgstr "Zjednodušená činština (Čína)"

#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr "Tradiční čínština (Thajsko)"

#: Language.class:241
msgid "Unknown"
msgstr "Neznámý"
