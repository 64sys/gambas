#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gbs3 3.18.90\n"
"PO-Revision-Date: 2023-06-06 22:31 UTC\n"
"Last-Translator: Martin Belmonte <info@belmotek.net>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# gb-ignore
#: .project:1
msgid "gbs3"
msgstr "-"

# gb-ignore
#: .project:2
msgid "Gambas Script"
msgstr "-"

#: CClassInfo.class:383
msgid "This class reimplements"
msgstr "Esta clase reimplementa "

#: CClassInfo.class:385
msgid "This class inherits"
msgstr "Esta clase hereda "

#: CClassInfo.class:392
msgid "in &1"
msgstr "en &1"

#: CClassInfo.class:400
msgid "This class is virtual."
msgstr "Esta clase es virtual."

#: CClassInfo.class:402
msgid "This class can be used like an object by creating a hidden instance on demand."
msgstr "Esta clase se puede usar como un objeto creando una instancia oculta bajo demanda."

#: CClassInfo.class:410
msgid "This class is &1."
msgstr "Esta clase es &1."

#: CClassInfo.class:410
msgid "creatable"
msgstr "instanciable"

#: CClassInfo.class:418
msgid "This class is static."
msgstr "Esta clase es estática."

#: CClassInfo.class:420
msgid "This class is not creatable."
msgstr "Esta clase no es instanciable."

#: CClassInfo.class:427
msgid "This class can be used as a &1."
msgstr "Esta clase se puede usar como un/una &1."

#: CClassInfo.class:427
msgid "function"
msgstr "función"

#: CClassInfo.class:427
msgid "static function"
msgstr "función estática"

#: CClassInfo.class:434
msgid "This class acts like a &1 / &2 static array."
msgstr "Esta clase actúa como una matriz estática de &1 / &2."

#: CClassInfo.class:434
msgid "read"
msgstr "lectura"

#: CClassInfo.class:434
msgid "write"
msgstr "escritura"

#: CClassInfo.class:436
msgid "This class acts like a &1 static array."
msgstr "Esta clase actúa com una matriz estática de &1."

#: CClassInfo.class:436
msgid "read-only"
msgstr "sólo lectura"

#: CClassInfo.class:440
msgid "This class acts like a &1 / &2 array."
msgstr "Esta clase actúa como una matriz de &1 / &2."

#: CClassInfo.class:442
msgid "This class acts like a &1 array."
msgstr "Esta clase actúa como una matriz de &1."

#: CClassInfo.class:448
msgid "This class is &1 with the &2 keyword."
msgstr "Esta clase es &1 con la palabra clave &2."

# gb-ignore
#: CClassInfo.class:448
msgid "enumerable"
msgstr "-"

#: CClassInfo.class:448
msgid "statically enumerable"
msgstr "enumerable estáticamente"

#: CClassInfo.class:457
msgid "Static properties"
msgstr "Propiedades estáticas"

#: CClassInfo.class:458
msgid "Static methods"
msgstr "Métodos estáticos"

#: CClassInfo.class:460
msgid "Constants"
msgstr "Constantes"

#: CClassInfo.class:484
msgid "Properties"
msgstr "Propiedades"

#: CClassInfo.class:485
msgid "Methods"
msgstr "Métodos"

#: CClassInfo.class:486
msgid "Events"
msgstr "Eventos"

#: CComponent.class:58
msgid "Graphical form management"
msgstr "Gestión gráfica de formulario"

#: CComponent.class:59
msgid "Event loop management"
msgstr "Gestión del lazo de eventos"

#: CComponent.class:60
msgid "Image loading and saving"
msgstr "Carga y guardado de imagen"

#: CComponent.class:61
msgid "OpenGL display"
msgstr "Visualización OpenGL"

#: CComponent.class:62
msgid "Complex numbers"
msgstr "Números complejos"

#: CComponent.class:63
msgid "XML management"
msgstr "Gestión XML"

#: CComponent.class:865
msgid "Class"
msgstr "Clase"

#: CComponent.class:867 OldWiki.module:123
msgid "Description"
msgstr "Descripción"

#: CComponent.class:903
msgid "This component is not stable yet."
msgstr "Este componente aún no es estable."

#: CComponent.class:905
msgid "This component is deprecated."
msgstr "Este componente es obsoleto."

#: CComponent.class:914
msgid "Author"
msgstr "Autor"

#: CComponent.class:916
msgid "Authors"
msgstr "Autores"

#: CComponent.class:926
msgid "Implements"
msgstr "Implementa"

#: CComponent.class:946
msgid "Requires"
msgstr "Requiere"

#: CComponent.class:974
msgid "Excludes"
msgstr "Excluye"

#: Main.module:1065
msgid "There are &1 classes and &2 symbols in all Gambas components."
msgstr "Hay &1 clases y &2 símbolos en todos los componentes de Gambas."

#: Main.module:1320
msgid "You must be logged in to view the last changes."
msgstr "Debes iniciar sesión para ver los últimos cambios."

#: Main.module:1735
msgid "Please upload a file"
msgstr "Cargue un archivo"

#: Main.module:1754
msgid "Run"
msgstr "Ejecutar"

#: Main.module:1755
msgid "Play"
msgstr "Reproducir"

#: Main.module:1756
msgid "Loading"
msgstr "Cargando"

#: Main.module:1791
msgid "Unable to download configuration file from GitLab"
msgstr "No se puede descargar el archivo de configuración de GitLab"

#: Main.module:1794
msgid "Unable to find build configuration for '&1'"
msgstr "No se puede encontrar la configuración de compilación para '&1'"

#: Main.module:1882
msgid "A parent of the page is already selected."
msgstr "Un padre de la página ya está seleccionado."

#: Main.module:1937
msgid "Cannot move selection into itself."
msgstr "No se puede mover la selección dentro de sí misma."

#: Main.module:1952
msgid "Destination already exists: &1"
msgstr "El destino ya existe: &1"

#: Main.module:1960
msgid "Cannot remove target directory: &1: &2"
msgstr "No se puede eliminar el directorio de destino: &1: &2"

#: Main.module:1968
msgid "Cannot move directory to: &1: &2"
msgstr "No se puede mover el directorio a: &1: &2"

#: Main.module:1976
msgid "Cannot link source to target: &1"
msgstr "No se puede enlazar el origen con el destino: &1"

#: Main.module:1996
msgid "Cannot purge a page having children."
msgstr "No se puede purgar una página que tiene hijos."

#: Main.module:2010
msgid "Cannot purge page: &1"
msgstr "No se puede eliminar una página: &1"

#: OldWiki.module:110
msgid "Errors"
msgstr "Errores"

#: OldWiki.module:112
msgid "Examples"
msgstr "Ejemplos"

#: OldWiki.module:114
msgid "See also"
msgstr "Ver también"

#: OldWiki.module:121
msgid "Message"
msgstr "Mensaje"

#: WikiMarkdown.class:172
msgid "Since"
msgstr "Desde"

#: WikiMarkdown.class:182
msgid "This component does not exist."
msgstr "Este componente no existe."

#: WikiMarkdown.class:207
msgid "This class does not exist."
msgstr "Esta clase no existe."

#: WikiMarkdown.class:220
msgid "This symbol does not exist."
msgstr "Este símbolo no existe."

#: WikiMarkdown.class:260
msgid "Enter a word to search through the wiki..."
msgstr "Introduce una palabra para buscar en el wiki..."

#: WikiMarkdown.class:264
msgid "Search is not available if you are not identified."
msgstr "La búsqueda no está disponible si no está identificado."

#: WikiMarkdown.class:280
msgid "Result"
msgstr "Resultado"

#: WikiMarkdown.class:294
msgid "No match found."
msgstr "No se ha encontrado ninguna coincidencia."

#: Wiki.webpage:11
msgid "Gambas Documentation"
msgstr "Documentación de Gambas"

# gb-ignore
#: Wiki.webpage:32
msgid "OK"
msgstr "-"

#: Wiki.webpage:74
msgid "Preview"
msgstr "Vista previa"

#: Wiki.webpage:75
msgid "Save"
msgstr "Guardar"

#: Wiki.webpage:76
msgid "Cancel"
msgstr "Cancelar"

# gb-ignore
#: Wiki.webpage:88
msgid "<"
msgstr "-"

# gb-ignore
#: Wiki.webpage:93
msgid ">"
msgstr "-"

#: Wiki.webpage:97
msgid "Exit"
msgstr "Salir"

#: Wiki.webpage:99
msgid "Edit"
msgstr "Editar"

#: Wiki.webpage:100
msgid "Delete"
msgstr "Borrar"

#: Wiki.webpage:101
msgid "Undo"
msgstr "Deshacer"

#: Wiki.webpage:103
msgid "Historic"
msgstr "Histórico"

#: Wiki.webpage:107
msgid "Create"
msgstr "Crear"

#: Wiki.webpage:110
msgid "Purge"
msgstr "Purgar"

#: Wiki.webpage:115
msgid "Select"
msgstr "Seleccionar"

#: Wiki.webpage:118
msgid "Unselect"
msgstr "Deseleccionar"

#: Wiki.webpage:119
msgid "Move selection"
msgstr "Mover selección"

#: Wiki.webpage:131
msgid "Logout"
msgstr "Salir"

#: Wiki.webpage:133
msgid "Login"
msgstr "Iniciar sesión"

#: Wiki.webpage:152
msgid "Password"
msgstr "Contraseña"

#: Wiki.webpage:192
msgid "Select the image file to upload..."
msgstr "Seleccionar archivo de imagen a cargar..."

#: Wiki.class:196
msgid "Warning! This is a preview. Click on the <b>&1</b> button to go back to the edit page."
msgstr "¡Atención! Esto es una vista previa. Haz clic en el botón <b>&1</b> para retroceder a la página de edición."

#: Wiki.class:245
msgid "This page does not exist in that language."
msgstr "Esta página no existe en ese lenguaje."

#: Wiki.class:267
msgid "This page does not exist."
msgstr "Esta página no existe."

#: Wiki.class:323
msgid "No change."
msgstr "Sin cambios."

#: Wiki.class:337
msgid "The english page is more recent."
msgstr "La página en inglés es más reciente."

